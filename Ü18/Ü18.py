{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Collecting tensorflow\n",
      "  Downloading tensorflow-2.8.0-cp38-cp38-win_amd64.whl (438.0 MB)\n",
      "Collecting termcolor>=1.1.0\n",
      "  Downloading termcolor-1.1.0.tar.gz (3.9 kB)\n",
      "Requirement already satisfied: numpy>=1.20 in c:\\users\\nico\\anaconda3\\lib\\site-packages (from tensorflow) (1.20.1)\n",
      "Collecting google-pasta>=0.1.1\n",
      "  Downloading google_pasta-0.2.0-py3-none-any.whl (57 kB)\n",
      "Collecting absl-py>=0.4.0\n",
      "  Downloading absl_py-1.0.0-py3-none-any.whl (126 kB)\n",
      "Requirement already satisfied: h5py>=2.9.0 in c:\\users\\nico\\anaconda3\\lib\\site-packages (from tensorflow) (2.10.0)\n",
      "Collecting keras-preprocessing>=1.1.1\n",
      "  Downloading Keras_Preprocessing-1.1.2-py2.py3-none-any.whl (42 kB)\n",
      "Collecting gast>=0.2.1\n",
      "  Downloading gast-0.5.3-py3-none-any.whl (19 kB)\n",
      "Collecting opt-einsum>=2.3.2\n",
      "  Downloading opt_einsum-3.3.0-py3-none-any.whl (65 kB)\n",
      "Collecting libclang>=9.0.1\n",
      "  Downloading libclang-13.0.0-py2.py3-none-win_amd64.whl (13.9 MB)\n",
      "Collecting tensorboard<2.9,>=2.8\n",
      "  Downloading tensorboard-2.8.0-py3-none-any.whl (5.8 MB)\n",
      "Collecting flatbuffers>=1.12\n",
      "  Downloading flatbuffers-2.0-py2.py3-none-any.whl (26 kB)\n",
      "Collecting tensorflow-io-gcs-filesystem>=0.23.1\n",
      "  Downloading tensorflow_io_gcs_filesystem-0.24.0-cp38-cp38-win_amd64.whl (1.5 MB)\n",
      "Collecting keras<2.9,>=2.8.0rc0\n",
      "  Downloading keras-2.8.0-py2.py3-none-any.whl (1.4 MB)\n",
      "Requirement already satisfied: wrapt>=1.11.0 in c:\\users\\nico\\anaconda3\\lib\\site-packages (from tensorflow) (1.12.1)\n",
      "Requirement already satisfied: six>=1.12.0 in c:\\users\\nico\\anaconda3\\lib\\site-packages (from tensorflow) (1.15.0)\n",
      "Collecting astunparse>=1.6.0\n",
      "  Downloading astunparse-1.6.3-py2.py3-none-any.whl (12 kB)\n",
      "Collecting tf-estimator-nightly==2.8.0.dev2021122109\n",
      "  Downloading tf_estimator_nightly-2.8.0.dev2021122109-py2.py3-none-any.whl (462 kB)\n",
      "Requirement already satisfied: setuptools in c:\\users\\nico\\anaconda3\\lib\\site-packages (from tensorflow) (52.0.0.post20210125)\n",
      "Requirement already satisfied: typing-extensions>=3.6.6 in c:\\users\\nico\\anaconda3\\lib\\site-packages (from tensorflow) (3.7.4.3)\n",
      "Collecting protobuf>=3.9.2\n",
      "  Downloading protobuf-3.19.4-cp38-cp38-win_amd64.whl (895 kB)\n",
      "Collecting grpcio<2.0,>=1.24.3\n",
      "  Downloading grpcio-1.44.0-cp38-cp38-win_amd64.whl (3.4 MB)\n",
      "Requirement already satisfied: wheel<1.0,>=0.23.0 in c:\\users\\nico\\anaconda3\\lib\\site-packages (from astunparse>=1.6.0->tensorflow) (0.36.2)\n",
      "Collecting markdown>=2.6.8\n",
      "  Downloading Markdown-3.3.6-py3-none-any.whl (97 kB)\n",
      "Requirement already satisfied: requests<3,>=2.21.0 in c:\\users\\nico\\anaconda3\\lib\\site-packages (from tensorboard<2.9,>=2.8->tensorflow) (2.25.1)\n",
      "Collecting google-auth-oauthlib<0.5,>=0.4.1\n",
      "  Downloading google_auth_oauthlib-0.4.6-py2.py3-none-any.whl (18 kB)\n",
      "Collecting tensorboard-plugin-wit>=1.6.0\n",
      "  Downloading tensorboard_plugin_wit-1.8.1-py3-none-any.whl (781 kB)\n",
      "Collecting google-auth<3,>=1.6.3\n",
      "  Downloading google_auth-2.6.2-py2.py3-none-any.whl (156 kB)\n",
      "Collecting tensorboard-data-server<0.7.0,>=0.6.0\n",
      "  Downloading tensorboard_data_server-0.6.1-py3-none-any.whl (2.4 kB)\n",
      "Requirement already satisfied: werkzeug>=0.11.15 in c:\\users\\nico\\anaconda3\\lib\\site-packages (from tensorboard<2.9,>=2.8->tensorflow) (1.0.1)\n",
      "Collecting cachetools<6.0,>=2.0.0\n",
      "  Downloading cachetools-5.0.0-py3-none-any.whl (9.1 kB)\n",
      "Collecting pyasn1-modules>=0.2.1\n",
      "  Downloading pyasn1_modules-0.2.8-py2.py3-none-any.whl (155 kB)\n",
      "Collecting rsa<5,>=3.1.4\n",
      "  Downloading rsa-4.8-py3-none-any.whl (39 kB)\n",
      "Collecting requests-oauthlib>=0.7.0\n",
      "  Downloading requests_oauthlib-1.3.1-py2.py3-none-any.whl (23 kB)\n",
      "Collecting importlib-metadata>=4.4\n",
      "  Downloading importlib_metadata-4.11.3-py3-none-any.whl (18 kB)\n",
      "Requirement already satisfied: zipp>=0.5 in c:\\users\\nico\\anaconda3\\lib\\site-packages (from importlib-metadata>=4.4->markdown>=2.6.8->tensorboard<2.9,>=2.8->tensorflow) (3.4.1)\n",
      "Collecting pyasn1<0.5.0,>=0.4.6\n",
      "  Downloading pyasn1-0.4.8-py2.py3-none-any.whl (77 kB)\n",
      "Requirement already satisfied: certifi>=2017.4.17 in c:\\users\\nico\\anaconda3\\lib\\site-packages (from requests<3,>=2.21.0->tensorboard<2.9,>=2.8->tensorflow) (2020.12.5)\n",
      "Requirement already satisfied: chardet<5,>=3.0.2 in c:\\users\\nico\\anaconda3\\lib\\site-packages (from requests<3,>=2.21.0->tensorboard<2.9,>=2.8->tensorflow) (4.0.0)\n",
      "Requirement already satisfied: idna<3,>=2.5 in c:\\users\\nico\\anaconda3\\lib\\site-packages (from requests<3,>=2.21.0->tensorboard<2.9,>=2.8->tensorflow) (2.10)\n",
      "Requirement already satisfied: urllib3<1.27,>=1.21.1 in c:\\users\\nico\\anaconda3\\lib\\site-packages (from requests<3,>=2.21.0->tensorboard<2.9,>=2.8->tensorflow) (1.26.4)\n",
      "Collecting oauthlib>=3.0.0\n",
      "  Downloading oauthlib-3.2.0-py3-none-any.whl (151 kB)\n",
      "Building wheels for collected packages: termcolor\n",
      "  Building wheel for termcolor (setup.py): started\n",
      "  Building wheel for termcolor (setup.py): finished with status 'done'\n",
      "  Created wheel for termcolor: filename=termcolor-1.1.0-py3-none-any.whl size=4829 sha256=a2543581b7085aa0623aa11731d652429a0e6e91f38c387037178eb1c00f5e03\n",
      "  Stored in directory: c:\\users\\nico\\appdata\\local\\pip\\cache\\wheels\\a0\\16\\9c\\5473df82468f958445479c59e784896fa24f4a5fc024b0f501\n",
      "Successfully built termcolor\n",
      "Installing collected packages: pyasn1, rsa, pyasn1-modules, oauthlib, cachetools, requests-oauthlib, importlib-metadata, google-auth, tensorboard-plugin-wit, tensorboard-data-server, protobuf, markdown, grpcio, google-auth-oauthlib, absl-py, tf-estimator-nightly, termcolor, tensorflow-io-gcs-filesystem, tensorboard, opt-einsum, libclang, keras-preprocessing, keras, google-pasta, gast, flatbuffers, astunparse, tensorflow\n",
      "  Attempting uninstall: importlib-metadata\n",
      "    Found existing installation: importlib-metadata 3.10.0\n",
      "    Uninstalling importlib-metadata-3.10.0:\n",
      "      Successfully uninstalled importlib-metadata-3.10.0\n",
      "Successfully installed absl-py-1.0.0 astunparse-1.6.3 cachetools-5.0.0 flatbuffers-2.0 gast-0.5.3 google-auth-2.6.2 google-auth-oauthlib-0.4.6 google-pasta-0.2.0 grpcio-1.44.0 importlib-metadata-4.11.3 keras-2.8.0 keras-preprocessing-1.1.2 libclang-13.0.0 markdown-3.3.6 oauthlib-3.2.0 opt-einsum-3.3.0 protobuf-3.19.4 pyasn1-0.4.8 pyasn1-modules-0.2.8 requests-oauthlib-1.3.1 rsa-4.8 tensorboard-2.8.0 tensorboard-data-server-0.6.1 tensorboard-plugin-wit-1.8.1 tensorflow-2.8.0 tensorflow-io-gcs-filesystem-0.24.0 termcolor-1.1.0 tf-estimator-nightly-2.8.0.dev2021122109\n"
     ]
    }
   ],
   "source": [
    "!pip install tensorflow"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "outputs": [],
   "source": [
    "from tensorflow.keras.datasets import mnist\n",
    "import pandas as pd\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "from xlrd.compdoc import x_dump_line"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   }
  },
  {
   "cell_type": "markdown",
   "source": [],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Downloading data from https://storage.googleapis.com/tensorflow/tf-keras-datasets/mnist.npz\n",
      "11493376/11490434 [==============================] - 4s 0us/step\n",
      "11501568/11490434 [==============================] - 4s 0us/step\n",
      "(60000, 28, 28)\n"
     ]
    }
   ],
   "source": [
    "(x_train, y_train), (x_test, y_test) = mnist.load_data()\n",
    "print(x_train.shape)"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   }
  },
  {
   "cell_type": "markdown",
   "source": [],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "outputs": [
    {
     "data": {
      "text/plain": "(60000, 784)"
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "df = pd.DataFrame(x_train.reshape(60000,784))\n",
    "df.shape"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   }
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "outputs": [
    {
     "data": {
      "text/plain": "       0    1    2    3    4    5    6    7    8    9    ...  774  775  776  \\\n0        0    0    0    0    0    0    0    0    0    0  ...    0    0    0   \n1        0    0    0    0    0    0    0    0    0    0  ...    0    0    0   \n2        0    0    0    0    0    0    0    0    0    0  ...    0    0    0   \n3        0    0    0    0    0    0    0    0    0    0  ...    0    0    0   \n4        0    0    0    0    0    0    0    0    0    0  ...    0    0    0   \n...    ...  ...  ...  ...  ...  ...  ...  ...  ...  ...  ...  ...  ...  ...   \n59995    0    0    0    0    0    0    0    0    0    0  ...    0    0    0   \n59996    0    0    0    0    0    0    0    0    0    0  ...    0    0    0   \n59997    0    0    0    0    0    0    0    0    0    0  ...    0    0    0   \n59998    0    0    0    0    0    0    0    0    0    0  ...    0    0    0   \n59999    0    0    0    0    0    0    0    0    0    0  ...    0    0    0   \n\n       777  778  779  780  781  782  783  \n0        0    0    0    0    0    0    0  \n1        0    0    0    0    0    0    0  \n2        0    0    0    0    0    0    0  \n3        0    0    0    0    0    0    0  \n4        0    0    0    0    0    0    0  \n...    ...  ...  ...  ...  ...  ...  ...  \n59995    0    0    0    0    0    0    0  \n59996    0    0    0    0    0    0    0  \n59997    0    0    0    0    0    0    0  \n59998    0    0    0    0    0    0    0  \n59999    0    0    0    0    0    0    0  \n\n[60000 rows x 784 columns]",
      "text/html": "<div>\n<style scoped>\n    .dataframe tbody tr th:only-of-type {\n        vertical-align: middle;\n    }\n\n    .dataframe tbody tr th {\n        vertical-align: top;\n    }\n\n    .dataframe thead th {\n        text-align: right;\n    }\n</style>\n<table border=\"1\" class=\"dataframe\">\n  <thead>\n    <tr style=\"text-align: right;\">\n      <th></th>\n      <th>0</th>\n      <th>1</th>\n      <th>2</th>\n      <th>3</th>\n      <th>4</th>\n      <th>5</th>\n      <th>6</th>\n      <th>7</th>\n      <th>8</th>\n      <th>9</th>\n      <th>...</th>\n      <th>774</th>\n      <th>775</th>\n      <th>776</th>\n      <th>777</th>\n      <th>778</th>\n      <th>779</th>\n      <th>780</th>\n      <th>781</th>\n      <th>782</th>\n      <th>783</th>\n    </tr>\n  </thead>\n  <tbody>\n    <tr>\n      <th>0</th>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>...</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n    </tr>\n    <tr>\n      <th>1</th>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>...</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n    </tr>\n    <tr>\n      <th>2</th>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>...</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n    </tr>\n    <tr>\n      <th>3</th>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>...</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n    </tr>\n    <tr>\n      <th>4</th>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>...</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n    </tr>\n    <tr>\n      <th>...</th>\n      <td>...</td>\n      <td>...</td>\n      <td>...</td>\n      <td>...</td>\n      <td>...</td>\n      <td>...</td>\n      <td>...</td>\n      <td>...</td>\n      <td>...</td>\n      <td>...</td>\n      <td>...</td>\n      <td>...</td>\n      <td>...</td>\n      <td>...</td>\n      <td>...</td>\n      <td>...</td>\n      <td>...</td>\n      <td>...</td>\n      <td>...</td>\n      <td>...</td>\n      <td>...</td>\n    </tr>\n    <tr>\n      <th>59995</th>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>...</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n    </tr>\n    <tr>\n      <th>59996</th>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>...</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n    </tr>\n    <tr>\n      <th>59997</th>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>...</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n    </tr>\n    <tr>\n      <th>59998</th>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>...</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n    </tr>\n    <tr>\n      <th>59999</th>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>...</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n      <td>0</td>\n    </tr>\n  </tbody>\n</table>\n<p>60000 rows × 784 columns</p>\n</div>"
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "df"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   }
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "outputs": [
    {
     "data": {
      "text/plain": "<matplotlib.image.AxesImage at 0x25ded38eb20>"
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    },
    {
     "data": {
      "text/plain": "<Figure size 432x288 with 1 Axes>",
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAPsAAAD4CAYAAAAq5pAIAAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjMuNCwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy8QVMy6AAAACXBIWXMAAAsTAAALEwEAmpwYAAAOUElEQVR4nO3dX4xUdZrG8ecFwT8MKiyt2zJEZtGYIRqBlLAJG0Qni38SBS5mAzGIxogXIDMJxEW5gAsvjO7MZBQzplEDbEYmhJEIiRkHCcYQE0OhTAuLLGpapkeEIkTH0QsU373ow6bFrl81VafqlP1+P0mnquup0+dNhYdTXae6fubuAjD0DSt6AACtQdmBICg7EARlB4Kg7EAQF7RyZ+PGjfOJEye2cpdAKD09PTp58qQNlDVUdjO7XdJvJQ2X9Ly7P5G6/8SJE1UulxvZJYCEUqlUNav7abyZDZf0rKQ7JE2WtNDMJtf78wA0VyO/s0+X9IG7f+TupyX9QdLcfMYCkLdGyj5e0l/7fd+b3fYdZrbEzMpmVq5UKg3sDkAjGin7QC8CfO+9t+7e5e4ldy91dHQ0sDsAjWik7L2SJvT7/seSPmlsHADN0kjZ90q61sx+YmYjJS2QtD2fsQDkre5Tb+7+jZktk/Sa+k69vejuB3ObDECuGjrP7u6vSno1p1kANBFvlwWCoOxAEJQdCIKyA0FQdiAIyg4EQdmBICg7EARlB4Kg7EAQlB0IgrIDQVB2IAjKDgRB2YEgKDsQBGUHgqDsQBCUHQiCsgNBUHYgCMoOBEHZgSAoOxAEZQeCoOxAEJQdCIKyA0FQdiCIhlZxRfs7c+ZMMv/888+buv9169ZVzb766qvktocPH07mzz77bDJfuXJl1Wzz5s3JbS+66KJkvmrVqmS+Zs2aZF6EhspuZj2SvpB0RtI37l7KYygA+cvjyH6Lu5/M4ecAaCJ+ZweCaLTsLunPZrbPzJYMdAczW2JmZTMrVyqVBncHoF6Nln2mu0+TdIekpWY269w7uHuXu5fcvdTR0dHg7gDUq6Gyu/sn2eUJSdskTc9jKAD5q7vsZjbKzEafvS5pjqQDeQ0GIF+NvBp/paRtZnb257zk7n/KZaoh5ujRo8n89OnTyfytt95K5nv27KmaffbZZ8ltt27dmsyLNGHChGT+8MMPJ/Nt27ZVzUaPHp3c9sYbb0zmN998czJvR3WX3d0/kpR+RAC0DU69AUFQdiAIyg4EQdmBICg7EAR/4pqDd999N5nfeuutybzZf2baroYPH57MH3/88WQ+atSoZH7PPfdUza666qrktmPGjEnm1113XTJvRxzZgSAoOxAEZQeCoOxAEJQdCIKyA0FQdiAIzrPn4Oqrr07m48aNS+btfJ59xowZybzW+ejdu3dXzUaOHJncdtGiRckc54cjOxAEZQeCoOxAEJQdCIKyA0FQdiAIyg4EwXn2HIwdOzaZP/XUU8l8x44dyXzq1KnJfPny5ck8ZcqUKcn89ddfT+a1/qb8wIHqSwk8/fTTyW2RL47sQBCUHQiCsgNBUHYgCMoOBEHZgSAoOxAE59lbYN68ecm81ufK11peuLu7u2r2/PPPJ7dduXJlMq91Hr2W66+/vmrW1dXV0M/G+al5ZDezF83shJkd6HfbWDPbaWZHssv0JxgAKNxgnsZvkHT7ObetkrTL3a+VtCv7HkAbq1l2d39T0qlzbp4raWN2faOkefmOBSBv9b5Ad6W7H5Ok7PKKanc0syVmVjazcqVSqXN3ABrV9Ffj3b3L3UvuXuro6Gj27gBUUW/Zj5tZpyRllyfyGwlAM9Rb9u2SFmfXF0t6JZ9xADRLzfPsZrZZ0mxJ48ysV9IaSU9I2mJmD0g6KunnzRxyqLv00ksb2v6yyy6re9ta5+EXLFiQzIcN431ZPxQ1y+7uC6tEP8t5FgBNxH/LQBCUHQiCsgNBUHYgCMoOBMGfuA4Ba9eurZrt27cvue0bb7yRzGt9lPScOXOSOdoHR3YgCMoOBEHZgSAoOxAEZQeCoOxAEJQdCILz7ENA6uOe169fn9x22rRpyfzBBx9M5rfccksyL5VKVbOlS5cmtzWzZI7zw5EdCIKyA0FQdiAIyg4EQdmBICg7EARlB4LgPPsQN2nSpGS+YcOGZH7//fcn802bNtWdf/nll8lt77333mTe2dmZzPFdHNmBICg7EARlB4Kg7EAQlB0IgrIDQVB2IAjOswc3f/78ZH7NNdck8xUrViTz1OfOP/roo8ltP/7442S+evXqZD5+/PhkHk3NI7uZvWhmJ8zsQL/b1prZ38xsf/Z1Z3PHBNCowTyN3yDp9gFu/427T8m+Xs13LAB5q1l2d39T0qkWzAKgiRp5gW6ZmXVnT/PHVLuTmS0xs7KZlSuVSgO7A9CIesv+O0mTJE2RdEzSr6rd0d273L3k7qWOjo46dwegUXWV3d2Pu/sZd/9W0npJ0/MdC0De6iq7mfX/28L5kg5Uuy+A9lDzPLuZbZY0W9I4M+uVtEbSbDObIskl9Uh6qHkjokg33HBDMt+yZUsy37FjR9XsvvvuS2773HPPJfMjR44k8507dybzaGqW3d0XDnDzC02YBUAT8XZZIAjKDgRB2YEgKDsQBGUHgjB3b9nOSqWSl8vllu0P7e3CCy9M5l9//XUyHzFiRDJ/7bXXqmazZ89ObvtDVSqVVC6XB1zrmiM7EARlB4Kg7EAQlB0IgrIDQVB2IAjKDgTBR0kjqbu7O5lv3bo1me/du7dqVus8ei2TJ09O5rNmzWro5w81HNmBICg7EARlB4Kg7EAQlB0IgrIDQVB2IAjOsw9xhw8fTubPPPNMMn/55ZeT+aeffnreMw3WBRek/3l2dnYm82HDOJb1x6MBBEHZgSAoOxAEZQeCoOxAEJQdCIKyA0Fwnv0HoNa57Jdeeqlqtm7duuS2PT099YyUi5tuuimZr169OpnffffdeY4z5NU8spvZBDPbbWaHzOygmf0iu32sme00syPZ5ZjmjwugXoN5Gv+NpBXu/lNJ/yppqZlNlrRK0i53v1bSrux7AG2qZtnd/Zi7v5Nd/0LSIUnjJc2VtDG720ZJ85o0I4AcnNcLdGY2UdJUSW9LutLdj0l9/yFIuqLKNkvMrGxm5Uql0uC4AOo16LKb2Y8k/VHSL93974Pdzt273L3k7qWOjo56ZgSQg0GV3cxGqK/ov3f3s38GddzMOrO8U9KJ5owIIA81T72ZmUl6QdIhd/91v2i7pMWSnsguX2nKhEPA8ePHk/nBgweT+bJly5L5+++/f94z5WXGjBnJ/JFHHqmazZ07N7ktf6Kar8GcZ58paZGk98xsf3bbY+or+RYze0DSUUk/b8qEAHJRs+zuvkfSgIu7S/pZvuMAaBaeJwFBUHYgCMoOBEHZgSAoOxAEf+I6SKdOnaqaPfTQQ8lt9+/fn8w//PDDekbKxcyZM5P5ihUrkvltt92WzC+++OLzngnNwZEdCIKyA0FQdiAIyg4EQdmBICg7EARlB4IIc5797bffTuZPPvlkMt+7d2/VrLe3t66Z8nLJJZdUzZYvX57cttbHNY8aNaqumdB+OLIDQVB2IAjKDgRB2YEgKDsQBGUHgqDsQBBhzrNv27atobwRkydPTuZ33XVXMh8+fHgyX7lyZdXs8ssvT26LODiyA0FQdiAIyg4EQdmBICg7EARlB4Kg7EAQ5u7pO5hNkLRJ0j9L+lZSl7v/1szWSnpQUiW762Pu/mrqZ5VKJS+Xyw0PDWBgpVJJ5XJ5wFWXB/Ommm8krXD3d8xstKR9ZrYzy37j7v+V16AAmmcw67Mfk3Qsu/6FmR2SNL7ZgwHI13n9zm5mEyVNlXT2M56WmVm3mb1oZmOqbLPEzMpmVq5UKgPdBUALDLrsZvYjSX+U9Et3/7uk30maJGmK+o78vxpoO3fvcveSu5c6OjoanxhAXQZVdjMbob6i/97dX5Ykdz/u7mfc/VtJ6yVNb96YABpVs+xmZpJekHTI3X/d7/bOfnebL+lA/uMByMtgXo2fKWmRpPfMbH9222OSFprZFEkuqUdSet1iAIUazKvxeyQNdN4ueU4dQHvhHXRAEJQdCIKyA0FQdiAIyg4EQdmBICg7EARlB4Kg7EAQlB0IgrIDQVB2IAjKDgRB2YEgan6UdK47M6tI+rjfTeMknWzZAOenXWdr17kkZqtXnrNd7e4Dfv5bS8v+vZ2bld29VNgACe06W7vOJTFbvVo1G0/jgSAoOxBE0WXvKnj/Ke06W7vOJTFbvVoyW6G/swNonaKP7ABahLIDQRRSdjO73cwOm9kHZraqiBmqMbMeM3vPzPabWaHrS2dr6J0wswP9bhtrZjvN7Eh2OeAaewXNttbM/pY9dvvN7M6CZptgZrvN7JCZHTSzX2S3F/rYJeZqyePW8t/ZzWy4pP+V9O+SeiXtlbTQ3f+npYNUYWY9kkruXvgbMMxslqR/SNrk7tdntz0p6ZS7P5H9RznG3f+zTWZbK+kfRS/jna1W1Nl/mXFJ8yTdpwIfu8Rc/6EWPG5FHNmnS/rA3T9y99OS/iBpbgFztD13f1PSqXNunitpY3Z9o/r+sbRcldnagrsfc/d3sutfSDq7zHihj11irpYoouzjJf213/e9aq/13l3Sn81sn5ktKXqYAVzp7sekvn88kq4oeJ5z1VzGu5XOWWa8bR67epY/b1QRZR9oKal2Ov83092nSbpD0tLs6SoGZ1DLeLfKAMuMt4V6lz9vVBFl75U0od/3P5b0SQFzDMjdP8kuT0japvZbivr42RV0s8sTBc/z/9ppGe+BlhlXGzx2RS5/XkTZ90q61sx+YmYjJS2QtL2AOb7HzEZlL5zIzEZJmqP2W4p6u6TF2fXFkl4pcJbvaJdlvKstM66CH7vClz9395Z/SbpTfa/IfyhpdREzVJnrXyT9Jfs6WPRskjar72nd1+p7RvSApH+StEvSkexybBvN9t+S3pPUrb5idRY027+p71fDbkn7s687i37sEnO15HHj7bJAELyDDgiCsgNBUHYgCMoOBEHZgSAoOxAEZQeC+D+ypTV9clByEAAAAABJRU5ErkJggg==\n"
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    }
   ],
   "source": [
    "plt.imshow(x_train[0], cmap='gray_r')"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   }
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}