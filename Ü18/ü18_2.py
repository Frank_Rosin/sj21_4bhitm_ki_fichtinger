#!/usr/bin/env python
# coding: utf-8

# In[2]:


import numpy as np
import tensorflow as tf
from tensorflow import keras
import pandas as pd
import matplotlib 
from matplotlib import pyplot as plt


# In[3]:


(x_train, y_train), (x_test, y_test) = tf.keras.datasets.mnist.load_data(path="mnist.npz")


# In[3]:


print(x_train.shape)
print(y_train.shape)
print(x_test.shape)
print(y_test.shape)
print()
print(x_train[0].shape)


# In[4]:


df = pd.DataFrame(np.reshape(x_train, (60000,784)))
df.head()


# In[5]:


plt.imshow(x_train[0], cmap="gray_r")
y_train[0]


# In[4]:


model = tf.keras.Sequential()

model.add(tf.keras.layers.Dense(512, activation="relu", input_shape=(784,)))
model.add(tf.keras.layers.Dense(10, activation="softmax"))

model.compile(optimizer="rmsprop", loss="categorical_crossentropy", metrics=["accuracy"]) 

model.summary()


# In[5]:


x_train = x_train.reshape(60000, 784)
# One-Hot-Encoding
y_train = tf.keras.utils.to_categorical(y_train, 10)


# In[8]:


print(x_train.shape)
print(y_train.shape)


# In[9]:


model.fit(x_train, y_train, epochs=5, batch_size=128)


# In[10]:


x_test = x_test.reshape(10000, 784)
# One-Hot-Encoding
y_test = tf.keras.utils.to_categorical(y_test, 10)


# In[11]:


print(x_test.shape)
print(y_test.shape)


# In[12]:


model.evaluate(x_test, y_test)


# ## vgl. RFC

# In[13]:


y_pred = model.predict(x_test)


# In[14]:


y_pred[2]


# In[15]:


y_test[1]


# # MNIST 2

# In[16]:


model_history = model.fit(x_train, y_train, epochs=15, batch_size=128, validation_data=(x_test, y_test))
type(model_history.history)
acc = model_history.history["accuracy"]
acc


# In[6]:


plt.plot(range(len(acc)), acc, 'b', label="Training") 
plt.title("Korrektklassifizierungsrate Training") 
plt.xlabel("Epochen") 
plt.ylabel("Korrektklassifizierungsrate")
plt.ylim(0.975,1)
plt.legend() 
plt.show()


# In[9]:


acc = model_history.history["accuracy"]
val = model_history.history["val_accuracy"]
plt.plot(range(len(acc)), acc, 'b', label="Training") 
plt.plot(range(len(val)), val, 'r', label="Validierung") 
plt.title("Korrektklassifizierungsrate Training/Validierung") 
plt.xlabel("Epochen") 
plt.ylabel("Korrektklassifizierungsrate") 
plt.ylim(0.9658, 1)
plt.legend( loc = 'center right')
plt.show()


# In[14]:


loss = model_history.history["loss"]
val = model_history.history["val_loss"]
plt.plot(range(len(acc)), loss, 'b', label="loss") 
plt.plot(range(len(val)), val, 'r', label="val") 
plt.title("Verlustfunktion") 
plt.xlabel("Epochen") 
plt.ylabel("loss") 
plt.ylim(0, 1)
plt.legend( loc = 'center right')
plt.show()


# In[15]:


model.save

