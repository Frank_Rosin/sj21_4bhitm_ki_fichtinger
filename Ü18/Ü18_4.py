#!/usr/bin/env python
# coding: utf-8

# In[21]:


from PIL import Image
import numpy as np
import tensorflow as tf
from tensorflow import keras
import pandas as pd
import matplotlib 
from matplotlib import pyplot as plt


# In[22]:


model = keras.models.load_model("seas")
def image(number):
    img = Image.open("img/" + number + ".png").convert("L") 
    img = np.array(img) 
    img = img.reshape((1, 784))
    img = img/255
    return img


# In[23]:


prediction = model.predict(image("4"))
print(np.argmax(prediction, axis=1))

prediction = model.predict(image("5"))
print(np.argmax(prediction, axis=1))

prediction = model.predict(image("9"))
print(np.argmax(prediction, axis=1))

