#!/usr/bin/env python
# coding: utf-8

# In[1]:


from tensorflow.keras.datasets import mnist
from tensorflow import keras
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from xlrd.compdoc import x_dump_line


# In[2]:


(x_train, y_train), (x_test, y_test) = keras.datasets.boston_housing.load_data(path="boston_housing.npz", test_split=0.2, seed=113)


# In[9]:


print(x_train.shape)
print(len(x_train))
x_train


# In[10]:


y_train


# In[11]:


model = keras.Sequential()
model.add(keras.layers.Dense(32, activation="relu", input_shape=(x_train.shape[1],)))
model.add(keras.layers.Dense(16, activation="relu"))
model.add(keras.layers.Dense(1))
model.compile(optimizer="adam", loss="mse", metrics=["mae"])
model.summary()


# In[12]:


model.fit(x_train, y_train, epochs=32, batch_size=8, verbose=0)


# In[13]:


model.evaluate(x_test, y_test)


# In[14]:


df_xtrain = pd.DataFrame(x_train)
df_xtrain.plot(kind="box", figsize=(15,10))


# In[15]:


df_xtest = pd.DataFrame(x_test)
df_xtest.plot(kind="box", figsize=(15,10))


# In[16]:


df_xtrain_new = (df_xtrain-df_xtrain.mean())/df_xtrain.std()
df_xtrain_new


# In[17]:


df_xtrain_new.plot(kind="box", figsize=(15,10))


# In[18]:


df_xtest_new = (df_xtest-df_xtest.mean())/df_xtest.std()
df_xtest_new


# In[19]:


df_xtest_new.plot(kind="box", figsize=(15,10))


# In[20]:


x_train = df_xtrain_new.to_numpy()
x_test = df_xtest_new.to_numpy()
model.fit(x_train, y_train, epochs=32, batch_size=8, verbose=0)


# In[21]:


model.evaluate(x_test, y_test)

