#!/usr/bin/env python
# coding: utf-8

# # 1. Fill the missing pieces
# Fill the `____` parts in the code below.

# In[7]:


words = ['PYTHON', 'JOHN', 'chEEse', 'hAm', 'DOE', '123']
upper_case_words = []

for x in words:
    if x.isupper():
        upper_case_words.append(x)

print(upper_case_words)


# In[6]:


assert upper_case_words == ['PYTHON', 'JOHN', 'DOE']


# # 2. Calculate the sum of dict values
# Calculate the sum of the values in `magic_dict` by taking only into account numeric values (hint: see [isinstance](https://docs.python.org/3/library/functions.html#isinstance)). 

# In[9]:


magic_dict = dict(val1=44, val2='secret value', val3=55.0, val4=1)


# In[27]:


# Your implementation
sum=0

for x in magic_dict.values():
    if isinstance(x,(int,float)):
        sum +=x
        
        
sum_of_values = sum
print(sum_of_values)


# In[32]:


assert sum_of_values == 100


# # 3. Create a list of strings based on a list of numbers
# The rules:
# * If the number is a multiple of five and odd, the string should be `'five odd'`
# * If the number is a multiple of five and even, the string should be `'five even'`
# * If the number is odd, the string is `'odd'`
# * If the number is even, the string is `'even'`

# In[33]:


numbers = [1, 3, 4, 6, 81, 80, 100, 95]


# In[71]:


# Your implementation
my_list = []
for x in numbers:
    if x % 2 == 0 and x % 5 == 0:
            my_list.append("five even")
    elif x % 2 != 0 and x % 5 == 0:
         my_list.append("five odd")
    elif x % 2 == 0:
            my_list.append("even")
    elif x % 2 != 0:
            my_list.append("odd")
        
        
print(my_list)


# In[70]:


assert my_list == ['odd', 'odd', 'even', 'even', 'odd', 'five even', 'five even', 'five odd']

