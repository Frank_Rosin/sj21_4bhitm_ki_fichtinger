#!/usr/bin/env python
# coding: utf-8

# ## Wiederholung Properties und Inheritance
# 
# ### Bsp. 04.1
# VervollstÃ¤ndigen Sie nachfolgende Klasse `Robot` mit dem Ziel der *Blacklist*-ÃœberprÃ¼fung. Generell gilt: Wird beim Instanziieren keine Name angegeben oder ist dieser in der Blacklist `_fobidden_names` angefÃ¼hrt, gilt es den *Default*-Namen *Marvin* zuzuweisen. In allen anderen FÃ¤llen ist der Name gemÃ¤ÃŸ Argument zu setzen.    

# In[2]:


class Robot:
    
    _forbidden_names = ["Charly", "Alan"]
    
    def __init__(self, name="Marvin"): #zum überladen
        self._name = name
        
    
    def get_name(self):
        return self._name
    
    
    def set_name(self, name):
        if name in Robot._forbidden_names:
            self._name = "Marvin"
        else:
            self._name = name


# **Testszenarien**: Zeigen Sie die Funktionsweise anhand nachfolgender Tests!

# In[3]:


r = Robot()
r.get_name()
# Output: 'Marvin'


# In[10]:


r2=Robot("Alan")
r2.get_name()
# Output: 'Marvin'


# In[5]:


r3=Robot("Markus")
r3.get_name()
# Output: 'Markus'


# ### Bsp. 04.2
# Durch den Einsatz der *Decorators* `@property` und `@...setter` kann man, wie es in Python Ã¼blich ist, via Attribut zugreifen - also "pythonsich". Adaptieren Sie den Code vom ersten Bsp.:

# In[13]:


class Robot:
    
    _forbidden_names = ["Charly", "Alan"]
    
    def __init__(self, name="Marvin"):
        self.name = name
        
    @property
    def name(self):
        return self._name
    
    @name.setter
    def name(self, name):
        if name in self._forbidden_names:
             self._name = "Marvin"
        else:
            self._name = name


# **Testszenario**:

# In[14]:


r = Robot("R2D2")
print(r.name)
r.name="D2R2"
print(r.name)


# ### Bsp. 04.3 - Inheritance
# Gegen ist die Klasse Robot mit folgender Implementierung:

# In[16]:


class Robot():
    
    def __init__(self, name="Marvin"):
        self.name=name
    
    
    def say_hello(self):
        return f"Hello, I'm {self.name}"


# `say_hello` liefert folgenden Output:

# In[17]:


r3 = Robot()
r3.say_hello()


# Die Klasse `MedicalRobot` ist von `Robot` abgeleitet und ergÃ¤nzt diese um die Methode `heal`, die einen beliebigen Namen als Argument Ã¼bernimmt und ausgibt.

# In[18]:


class MedicalRobot(Robot):
    
    def heal(self, name):
        return f"{name} is healed now, or maybe not!"


# Ãœberschreiben Sie in der Klasse `MedicalRobot` die Methode `say_hello` mit dem Ziel, dass nachfolgende Tests das definierte Ergebnis liefern.

# In[19]:


class MedicalRobot(Robot):
    
    def heal(self, x):
        return f"{x} is healed now, or maybe not!"
    
    def say_hello(self):
        return f"Hello, I'm {self.name}, your personal nurse!"


# **Tests**:

# In[20]:


mr = MedicalRobot("Kelvin")
print(mr.say_hello())
# Output: Hello, I'm Kelvin, your personal nurse!

