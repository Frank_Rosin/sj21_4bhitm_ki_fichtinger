#!/usr/bin/env python
# coding: utf-8

# In[1]:


get_ipython().system('pip install tensorflow')


# In[3]:


from tensorflow.keras.datasets import mnist
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from xlrd.compdoc import x_dump_line


# 

# In[4]:


(x_train, y_train), (x_test, y_test) = mnist.load_data()
print(x_train.shape)


# 

# In[5]:


df = pd.DataFrame(x_train.reshape(60000,784))
df.shape


# In[7]:


df


# In[8]:


plt.imshow(x_train[0], cmap='gray_r')

