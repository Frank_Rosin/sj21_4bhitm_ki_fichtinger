#!/usr/bin/env python
# coding: utf-8

# 

# In[1]:


import pandas as pd
from astropy.stats import histogram_intervals
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split

import seaborn as sns
from xlsxwriter import chart_bar


# In[2]:


df = pd.read_csv("diabetes.csv")
df


# In[3]:


sns.countplot(x='Outcome', data=df)


# In[4]:


correlation = df.corr()


# In[5]:


sns.heatmap(correlation, vmin=0, vmax=1, cmap="Blues_r")

