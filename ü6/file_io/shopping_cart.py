#!/usr/bin/env python
# coding: utf-8

# In[1]:


def totalprice1(input_file):
    with open(input_file) as file:
        total=0
        lines = file.readlines()
        f = open("shopping_cart.txt", "r")
        for line in lines[1:]:
            s=line.split(';')
            total += float(s[1])*float(s[2])
        print(total)
        return total
    
def totalprice2(input_file):
     with open(input_file) as file:
        total=0
        lines = file.readlines()
        f = open("shopping_cart2.csv", "r")
        for line in lines[1:]:
            s=line.split(';')
            total += float(s[1])
        print(total)
        return total


# In[4]:


with open("shopping_cart.txt") as file:
       new = "shopping_cart2.txt"
       new_file = open(new, 'w+')
       lines = file.readlines()
       new_file.write(lines[0].replace('Menge;Einzelpreis', 'Gesamtpreis'))
       maxsum = 0
       for line in lines[1:]:
           text = line.split(';')
           sum = float(text[1]) * float(text[2])
           s = f'{text[0]};{sum};{text[3]}'
           new_file.write(s)
 


# In[6]:


assert totalprice1("shopping_cart.txt") == totalprice2("shopping_cart2.txt")

