#!/usr/bin/env python
# coding: utf-8

# # Confusion Matrix 1
# 
# Die Erstellung der *Confusion Matrix* kann entweder mit *Scikit Learn* (https://scikit-learn.org/stable/modules/generated/sklearn.metrics.confusion_matrix.html) oder mit *pandas_ml* (https://pandas-ml.readthedocs.io/en/latest/conf_mat.html) erfolgen. Scikit Learn sollte installiert sein. pandas_ml kann mit folgendem Befehl installiert werden: `conda install -c conda-forge pandas_ml`. Den jeweiligen Import nicht vergessen! Im Rahmen dieser Übung kommt die Scikit Learn-Variante zum Einsatz.
# 
# ## Umsetzung mit Scikit Learn

# In[1]:


from sklearn.metrics import confusion_matrix
import pandas as pd
import sklearn
from sklearn import datasets
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score


# In[7]:


# actual values
actual = [1,0,0,1,0,0,1,0,0,1]

# predicted values
predicted = [1,0,0,1,0,0,0,1,0,0]

confusion_matrix(actual, predicted, labels=[1,0])


# **Default-Output von `confusion_matrix`:**
#         
#                Predicted
#                  0    1
#            0     5    1
#     Actual    
#            1     2    2
#            
# > Details liefert die API-Doku: **labels**:...If None is given, those that appear at least once in y_true or y_pred are used in sorted order.
# 
# **Output anpassen:**
# Möchte man mit '1' (Positiv) beginnen, steht das Argument `labels` zur Verfügung:

# In[8]:


data ={
    "predicted":predicted,
    "actual":actual
}

df = pd.DataFrame(data)
df["correct"] = df.apply(lambda x: 1 if x[0] == x[1] else 0, axis=1)
df 


# ## Task 16.1
# Grundlage bildet Übung 14, Heart Dataset in Kombintion mit dem Random Forest Classifier: Trainieren Sie den Random Forest Classifier und erstellen Sie die Confusion Matrix (CM) wie gezeigt.

# In[3]:


df = pd.read_csv("heart.csv")


df["Sex"] = df["Sex"].apply(lambda sex: 0 if sex == "M" else 1)
df["ChestPainType"] = df["ChestPainType"].apply(lambda pain: 0 if pain == "ATA" else 1 if pain == "NAP" else 2 if pain == "ASY" else 3)
df["RestingECG"] = df["RestingECG"].apply(lambda rest: 0 if rest == "Normal" else 1 if rest == "ST" else 2 )
df["ExerciseAngina"] = df["ExerciseAngina"].apply(lambda angina: 0 if angina == "Y" else 1)
df["ST_Slope"] = df["ST_Slope"].apply(lambda slope: 0 if slope == "Up" else 1 if slope == "Flat" else 2)
df


# In[6]:


clf = RandomForestClassifier(max_depth=50, n_estimators=500)


x = df.drop("HeartDisease", axis=1)
y = df["HeartDisease"]

x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=.3, random_state = 5)
clf.fit(x_train, y_train)
y_pred = clf.predict(x_test)

data = {
    "predicted": y_pred,
    "actual" : y_test
}
result_df = pd.DataFrame(data)
result_df["correct"] = result_df.apply(lambda x: 1 if x[0] == x[1] else 0, axis=1)
result_df

confusion_matrix(y_test, y_pred, labels=[1,0])

