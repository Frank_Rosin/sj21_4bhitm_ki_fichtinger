#!/usr/bin/env python
# coding: utf-8

# In[35]:


import pandas as pd
import sklearn
import numpy as np
from sklearn import datasets
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score


# In[36]:


#Create classifier of type "Random Forest"


# In[37]:


clf = RandomForestClassifier(max_depth=5,n_estimators=10)
clf


# In[39]:


iris = datasets.load_iris()

x = pd.DataFrame(iris.data, columns=iris.feature_names)
y = pd.Series(iris['target'])

print(x.head())
print(y.head())


# In[41]:


x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=.3)
clf.fit(x_train, y_train)
y_pred = clf.predict(x_test)
print(y_pred)


# In[44]:


data = {     
'predicted' : y_pred,   
'actual' : y_test 
}

result_df = pd.DataFrame(data)
result_df["correct"] = result_df.apply(lambda x:1 if x[0] == x[1] else 0, axis=1)

print(result_df)


# In[49]:


acc = result_df["correct"].mean() * 100
print(f"Genauigkeit: {acc}%")


# In[ ]:


# max_depth legt fest, wie tief der Baum verschachtelt werden kann
# n_estimators ist die Anzahl der verwendeten Bäume


# In[51]:


new_predict = clf.predict([[6, 3.0, 5.5, 1.8]])

#print(new_predict)
new_predict

