#!/usr/bin/env python
# coding: utf-8

# In[4]:


import pandas as pd
import sklearn
from sklearn import datasets
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score


# In[78]:


df = pd.read_csv("heart.csv")


df["Sex"] = x["Sex"].apply(lambda sex: 0 if sex == "M" else 1)
df["ChestPainType"] = x["ChestPainType"].apply(lambda pain: 0 if pain == "ATA" else 1 if pain == "NAP" else 2 if pain == "ASY" else 3)
df["RestingECG"] = x["RestingECG"].apply(lambda rest: 0 if rest == "Normal" else 1 if rest == "ST" else 2 )
df["ExerciseAngina"] = x["ExerciseAngina"].apply(lambda angina: 0 if angina == "Y" else 1)
df["ST_Slope"] = x["ST_Slope"].apply(lambda slope: 0 if slope == "Up" else 1 if slope == "Flat" else 2)
df


# In[140]:




clf = RandomForestClassifier(max_depth=50, n_estimators=500)


x = df.drop("HeartDisease", axis=1)
y = df["HeartDisease"]

x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=.3, random_state = 5)
clf.fit(x_train, y_train)
y_pred = clf.predict(x_test)

data = {
    "predicted": y_pred,
    "actual" : y_test
}
result_df = pd.DataFrame(data)
result_df["correct"] = result_df.apply(lambda x: 1 if x[0] == x[1] else 0, axis=1)
result_df


# In[141]:


accuracy = accuracy_score(y_test, y_pred, normalize=True)

print(accuracy)

