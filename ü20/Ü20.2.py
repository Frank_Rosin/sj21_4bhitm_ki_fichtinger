#!/usr/bin/env python
# coding: utf-8

# # Convolutional Neuronales Netzwerk – Übung 20 CNN2
# ---- 

# ##  20.2.1
# ----

# In[8]:


from tensorflow.keras.datasets import mnist
from tensorflow import keras
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from xlrd.compdoc import x_dump_line
import os


# In[9]:


os.listdir(os.path.join(os.getcwd(), "train"))


# In[10]:


df_train = pd.DataFrame(os.listdir(os.path.join(os.getcwd(), "train")), columns=["filename"])
df_train["category"] = df_train.apply(lambda x: x[0].split(".")[0], axis=1)


# In[7]:


df_train


# ##  20.2.2
# ----

# In[18]:


df_train.shape[0] #25000


# In[22]:


df_train["category"].value_counts() #ausgewogen


# ##  20.2.3
# ----

# In[38]:


def load(folder, img_type, imageid):
    if(os.path.exists(os.path.join(os.getcwd(), f"train\\{img_type}.{imageid}.jpg"))):
       image = keras.preprocessing.image.load_img(os.path.join(os.getcwd(), f"train\\{img_type}.{imageid}.jpg"))
    return image

plt.imshow(load('train',"cat",1)) 
plt.show()

plt.imshow(load('train',"dog",1)) 
plt.show()

