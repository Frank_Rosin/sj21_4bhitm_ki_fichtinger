#!/usr/bin/env python
# coding: utf-8

# # Convolutional Neuronales Netzwerk – Übung 20 CNN3
# ---- 

# In[11]:


#pip install --upgrade tensorflow
#pip install tf-nightly


# In[1]:


from tensorflow.keras.datasets import mnist
from tensorflow import keras
from tensorflow.keras.applications import MobileNet
from tensorflow.keras.applications import imagenet_utils
from IPython.display import Image
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from keras.models import Model
from keras.layers import Dense,GlobalAveragePooling2D
from keras.applications.mobilenet import preprocess_input
from keras.preprocessing.image import ImageDataGenerator


# In[2]:


def prepare_image(file):
    img_path = 'data/'
    img = keras.preprocessing.image.load_img(img_path + file, target_size=(224, 224))
    img_array = keras.preprocessing.image.img_to_array(img)
    img_array_expanded_dims = np.expand_dims(img_array, axis=0)
    return keras.applications.mobilenet.preprocess_input(img_array_expanded_dims)


# ##  20.3.1
# ----

# In[3]:


mobilenet = MobileNet()


# In[5]:


img = Image(filename='data/hund.jpg', width=250,height=300)
img


# In[7]:


preprocessed_image = prepare_image('hund.jpg')
predictions = mobilenet.predict(preprocessed_image)
results = imagenet_utils.decode_predictions(predictions)
results


# In[9]:


img = Image(filename='data/auto.jpg', width=250,height=300)
img


# In[11]:


preprocessed_image = prepare_image('auto.jpg')
predictions = mobilenet.predict(preprocessed_image)
results = imagenet_utils.decode_predictions(predictions)
results


# In[13]:


img = Image(filename='data/ratt.jpg', width=250,height=300)
img


# In[14]:


preprocessed_image = prepare_image('ratt.jpg')
predictions = mobilenet.predict(preprocessed_image)
results = imagenet_utils.decode_predictions(predictions)
results


# ##  20.3.2
# ----

# #### step 1

# In[15]:


base_model=MobileNet(weights='imagenet',include_top=False) #imports the mobilenet model and discards the last 1000 neuron layer.

x=base_model.output
x=GlobalAveragePooling2D()(x)
x=Dense(1024,activation='relu')(x) #we add dense layers so that the model can learn more complex functions and classify for better results.
x=Dense(1024,activation='relu')(x) #dense layer 2
x=Dense(512,activation='relu')(x) #dense layer 3
preds=Dense(3,activation='softmax')(x) #final layer with softmax activation


# In[16]:


model=Model(inputs=base_model.input,outputs=preds)
#specify the inputs
#specify the outputs
#now a model has been created based on our architecture


# In[17]:


for layer in model.layers[:20]:
    layer.trainable=False
for layer in model.layers[20:]:
    layer.trainable=True


# In[18]:


train_datagen=ImageDataGenerator(preprocessing_function=preprocess_input) #included in our dependencies

train_generator=train_datagen.flow_from_directory('./data/train', # this is where you specify the path to the main data folder
                                                 target_size=(224,224),
                                                 color_mode='rgb',
                                                 batch_size=32,
                                                 class_mode='categorical',
                                                 shuffle=True)


# In[17]:


model.compile(optimizer='Adam',loss='categorical_crossentropy',metrics=['accuracy'])
# Adam optimizer
# loss function will be categorical cross entropy
# evaluation metric will be accuracy

step_size_train=train_generator.n//train_generator.batch_size
model.fit_generator(generator=train_generator,
                   steps_per_epoch=step_size_train,
                   epochs=5)


# ##  20.3.3
# ----

# In[27]:


# 0 - cat
# 1 - dog
# 2 - horse


# In[29]:


preprocessed_image = prepare_image('pferdi.jpg')
prediction = model.predict(preprocessed_image)
prediction = np.argmax(prediction)
prediction


# In[30]:


preprocessed_image = prepare_image('hund.jpg')
prediction = model.predict(preprocessed_image)
prediction = np.argmax(prediction)
prediction


# In[31]:


preprocessed_image = prepare_image('katz.jpg')
prediction = model.predict(preprocessed_image)
prediction = np.argmax(prediction)
prediction

