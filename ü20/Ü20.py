#!/usr/bin/env python
# coding: utf-8

# In[1]:


from tensorflow.keras.datasets import mnist
from tensorflow import keras
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from xlrd.compdoc import x_dump_line


# In[2]:


(x_train, y_train), (x_test, y_test) = mnist.load_data()
print(x_train.shape)


# In[3]:


model = keras.Sequential()
#Erste Konvolutionsschicht:
model.add(keras.layers.Conv2D(32, kernel_size=(3,3), activation="relu",
                 input_shape=(28,28,1)))
#Zweite Konvolutionsschicht:
model.add(keras.layers.Conv2D(64, kernel_size=(3,3), activation="relu"))
model.add(keras.layers.MaxPooling2D(pool_size=(2,2)))
model.add(keras.layers.Dropout(0.25))
model.add(keras.layers.Flatten())
#Vollständig verbundene Schicht mit Dropout:
model.add(keras.layers.Dense(128, activation="relu"))
model.add(keras.layers.Dropout(0.25))
#Ausgabeschicht:
model.add(keras.layers.Dense(10, activation="softmax"))
model.compile(optimizer="rmsprop", loss="categorical_crossentropy",
              metrics=["accuracy"])


# In[4]:


x_train = x_train.reshape(x_train.shape[0],28,28,1)
x_test = x_test.reshape(x_test.shape[0],28,28,1)
print(x_train.shape)
print(x_test.shape)


# In[5]:


y_train = keras.utils.to_categorical(y_train, 10)
print(y_train.shape)


# In[6]:


model.fit(x_train, y_train, epochs=10, batch_size=1000) 


# In[7]:


model.save('model')


# In[8]:


import tensorflow.keras.backend as K 
data = K.eval(model.layers[0].weights[0]) 


# In[9]:


plt.imshow(data[:, :, :, 1].reshape(3,3), cmap="Greys") 
plt.show()

